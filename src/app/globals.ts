import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    siteTitle: string = "Whatstudy";
}
