import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class ChatService {

  user: any = (this.cookieService.check('user')) ? JSON.parse(this.cookieService.get('user')) : false;

  constructor(
    private restangular: Restangular,
    private cookieService: CookieService
  ) { }

  /**
   * Get all rooms from the api
   */
  public getRooms() {

    let params = {
      api_token: this.user.api_token
    };
    
    let base = this.restangular.all('rooms');

    return base.get("", params);

  }

  /**
   * Get messages from one chatroom
   * @param {number} id
   */
  public getChatroom(id: number) {

    let params = {
      api_token: this.user.api_token
    };
    
    let base = this.restangular.all('messages');

    return base.get(id, params);

  }

  /**
   * Send a message in a chatroom
   * @param {umber} id
   * @param {string} message
   */
  public sendMessage(id: number, message: string) {

    let params = {
      room_id: id,
      message: message,
      api_token: this.user.api_token
    };
    
    let base = this.restangular.all('messages');

    return base.post(id, params);

  }

  /**
   * Get available user types
   */
  public getUserTypes() {

    let params = {
      api_token: this.user.api_token
    };
    
    let base = this.restangular.all('user_types');

    return base.get('', params);

  }

}
