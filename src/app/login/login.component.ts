import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';

import { AuthService } from '../auth/auth.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;
  errorDialogRef: MatDialogRef<ErrorDialogComponent>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private cookieService: CookieService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    // Set return url to the route the user tried to visit, or the root of the site
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    // user logout
    setTimeout(() => {
      this.authService.logout();
    }, 500);
  }

  /**
   * Login function
   * Uses this.model for form data
   * On success create the user token from the api returned data
   * On erro show an error dialog with the message we got from the api
   */
  login() {
    this.loading = true;
    this.authService.login(this.model.username, this.model.password).subscribe(
    data => {

      // create json formatted token
      let token = {
        api_token: data.api_token,
        id: data.id,
        name: data.name,
        number: data.number,
        user_type_id: data.user_type_id
      }

      // set the user cookie
      this.cookieService.set('user', JSON.stringify(token));

      // return the user to the route corresponding to the return url
      this.router.navigate([this.returnUrl]);
    },
    error => {
      this.errorDialogRef = this.dialog.open(ErrorDialogComponent,{
        width: '250px',
        data: {
          error: error.data
        }
      });
      this.loading = false;
    });
  }

}
