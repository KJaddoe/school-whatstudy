import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatToolbarModule,
  MatTabsModule,
  MatCardModule,
  MatIconModule,
  MatMenuModule,
  MatSidenavModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatInputModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollbarModule } from 'ngx-scrollbar';

import { AppRoutingModule } from './app-routing.module';

/**
 Components
*/
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component'
import { ChatroomComponent } from './chatroom/chatroom.component';

import { AuthGuardService as AuthGuard } from './auth/auth-guard.service'
import { AuthService } from './auth/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { ChatService } from './chat.service';
import { LogoutComponent } from './logout/logout.component';

// Function for setting the default restangular configuration
export function RestangularConfigFactory (RestangularProvider) {
  let base = 'https://whatstudy-api.clow.nl/v1';
  RestangularProvider.setBaseUrl(base);
}

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FooterComponent,
    NotFoundComponent,
    LoginComponent,
    ErrorDialogComponent,
    ChatroomComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatInputModule,
    MatSidenavModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    FormsModule,
    FlexLayoutModule,
    ScrollbarModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    AppRoutingModule
  ],
  providers: [
    CookieService,
    ChatService,
    AuthGuard,
    AuthService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ErrorDialogComponent
  ]
})
export class AppModule { }
