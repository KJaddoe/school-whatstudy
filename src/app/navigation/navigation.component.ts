import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals';

import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  providers: [Globals]
})
export class NavigationComponent implements OnInit {

  userData: any;
  user: boolean;

  constructor(
    private globals: Globals,
    private cookieService: CookieService
  ) { }

  ngOnInit() {

    // check if user cookie exists (true/ false)
    this.user = this.cookieService.check('user');

    // if the user exists, this.userData equals a json parse of the saved cookie
    if (this.user) {
      this.userData = JSON.parse(this.cookieService.get('user'));
    }
  }

}
