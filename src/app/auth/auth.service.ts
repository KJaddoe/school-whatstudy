import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Restangular } from 'ngx-restangular';

import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {

  constructor(
    private restangular: Restangular,
    private cookieService: CookieService
  ) { }

  /**
   * Send login request to the api
   *
   * @param {string} username
   * @param {string} password
   *
   * @return observable
   */
  public login(username: string, password: string) {

    let params = {
      number: username,
      password: password,
    };

    let base = this.restangular.all('token');

    return base.get("", params);

  }

  /**
   * Logout user
   * Reload the page if the cookie still exists after deletion
   */
  public logout() {
    this.cookieService.delete('user');
    if (this.cookieService.check('user')) {
      alert('your page will be reloaded!');
      window.location.reload();
    } else {

    }
  }

}
