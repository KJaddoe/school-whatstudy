import { Component, ViewChild, NgZone } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ErrorDialogComponent } from './error-dialog/error-dialog.component';

import { ChatService } from './chat.service';
import { CookieService } from 'ngx-cookie-service';

import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Whatstudy';
  rooms: any;
  model: any = {};
  routeParams: any;
  userLoggedIn: boolean;
  loading: boolean;
  errorDialogRef: MatDialogRef<ErrorDialogComponent>;

  constructor(
    private router: Router,
    private chatService: ChatService,
    private dialog: MatDialog,
    private cookieService: CookieService,
    private ngZone: NgZone
  ) { }

  ngOnInit() {

    this.userLoggedIn = this.cookieService.check('user');

    // Do after route change
    this.router.events
    .filter(event => event instanceof NavigationEnd)
    .subscribe((event: NavigationEnd) => {
      this.loading = true;
      setTimeout(() =>{
        this.loading = false;
      }, 1000);
      setTimeout(() => {
        this.userLoggedIn = this.cookieService.check('user');
      }, 500);
    });

    if (this.userLoggedIn) {
      this.getChatrooms();
    }

  }

  /**
   * Get all available rooms from the api
   * On success set this.rooms to the response
   * On error open an error message is given
   */
  getChatrooms() {
    if (this.userLoggedIn) {
      this.chatService.getRooms().subscribe(
        data => {
          this.rooms = data;
        },
        error => {
          if (error.status === 401 && error.data.message === "Unauthorized") {
            this.errorDialogRef = this.dialog.open(ErrorDialogComponent,{
              width: '250px',
              data: {
                error: "Session expired, please log in again",
                status: error.status
              }
            });
          }
        }
      );
    }
  }

}
