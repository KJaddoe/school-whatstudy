import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ChatService } from '../chat.service';
import { CookieService } from 'ngx-cookie-service';

import { ScrollbarComponent } from 'ngx-scrollbar';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.scss']
})
export class ChatroomComponent implements OnInit {

  model: any = {};
  chatroom: any;

  intervalID: number;
  endPage: boolean;

  user: any = JSON.parse(this.cookieService.get('user'));
  userTypes: any;

  errorDialogRef: MatDialogRef<ErrorDialogComponent>;

  @ViewChild(ScrollbarComponent) scrollRef: ScrollbarComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private chatService: ChatService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {

    // get initial data on page load
    this.getUserTypes();
    this.getChatroom();

    // scroll to the bottom of the page on page load
    setTimeout(() => {
      this.scrollRef.scrollYTo(this.scrollRef.viewRef.nativeElement.scrollHeight);
    }, 500);

    // check every 1ms is we are at the bottom of the page
    setInterval(() => {
      this.scrollRef.scrollState.subscribe(state => {
        this.endPage = ((state.target.clientHeight + state.target.scrollTop) === state.target.scrollHeight) ? true: false;
      });
    }, 1);
    
    // scroll to the bottom of the page on a route change
    this.router.events
    .filter(event => event instanceof NavigationEnd)
    .subscribe((event: NavigationEnd) => {
      setTimeout(() => {
        this.scrollRef.scrollYTo(this.scrollRef.viewRef.nativeElement.scrollHeight);
      }, 500);
    });


  }

  /**
   * Get all chatrooms from the api
   * On sucess bind the returned api data to this.chatroom in reverse order
   * Add user role per message to user
   * On error open a dialog with a error message
   *
   * Clear the interval with the id equal to this.intervalID
   * Set this.intervalID to a new interval
   * Execute interval every 5 second
   * Interval executes this.getChatroom and this.scrollToBottom
   */
  getChatroom() {
    this.route.params.subscribe(res => {
      this.chatService.getChatroom(res.id).subscribe(
        data => {
          this.chatroom = data.reverse();
          for (let message of this.chatroom) {
            for (let userType of this.userTypes) {
              if (message.user_type_id === userType.id) {
                message.user_role = userType.name;
              }
            }
          }
        },
        error => {
          this.openDialog(error);
        }
      );
    });
    clearInterval(this.intervalID);
    this.intervalID = setInterval(() => {
      this.getChatroom();
      this.scrollToBottom();
    }, 5000);
  }

  /**
   * Get all user types
   * On success set this.userTypes to the api retuned data
   * On error open a dialog with a message
   */
  getUserTypes() {
    this.chatService.getUserTypes().subscribe(
      data => {
        this.userTypes = data;
      },
      error => {
        this.openDialog(error);
      }
    );
  }

  /**
   * Scroll to the bottom of the page
   * Only do this when we are at the bottom of the page
   */
  scrollToBottom() {
    if (this.endPage) {
      this.scrollRef.scrollYTo(this.scrollRef.viewRef.nativeElement.scrollHeight);
    }
  }

  /**
   * Send message
   * On success clear message model en get all chatroom messages
   * On error open a dialog with a message
   */
  sendMessage() {
    this.route.params.subscribe(res => {
      this.chatService.sendMessage(res.id, this.model.message).subscribe(
        data => {
          this.model = {};
          this.getChatroom();
        },
        error => {
          this.openDialog(error);
        }
      );
    });
  }

  /**
   * Open a dialog
   * @param {object} error
   */
  openDialog(error: any) {
    this.errorDialogRef = this.dialog.open(ErrorDialogComponent,{
      width: '250px',
      data: error
    });
  }

}
